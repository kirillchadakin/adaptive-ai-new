package mord.ai;

import mord.ai.execution.ExecutionManager;
import org.junit.jupiter.api.Test;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.*;

class ExecutionManagerTest {

    @Test
    void test() {
        AtomicInteger val = new AtomicInteger(0);
        ExecutionManager executionManager = new ExecutionManager(2);
        int incs = 100_000;

        IntStream.range(0, incs).forEach(i -> executionManager.schedule(val::incrementAndGet));
        executionManager.tick();
        assertEquals(incs, val.get());

        IntStream.range(0, incs).forEach(i -> executionManager.schedule(val::incrementAndGet));
        executionManager.tick();
        assertEquals(incs * 2, val.get());

        IntStream.range(0, incs).forEach(i -> executionManager.schedule(val::incrementAndGet));
        executionManager.tick();
        assertEquals(incs * 3, val.get());
    }
}