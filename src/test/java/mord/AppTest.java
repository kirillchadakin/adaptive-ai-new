package mord;

import lombok.extern.log4j.Log4j2;
import mord.ai.execution.NeuronExecutionManager;
import mord.ai.NeuronI;
import mord.ai.link.Link;
import mord.ai.link.LinkFactory;
import mord.ai.stat.ConstStatisticRule;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.concurrent.atomic.AtomicBoolean;

@Log4j2
public class AppTest {
    private NeuronExecutionManager executionManager;
    private AtomicBoolean d1;
    private AtomicBoolean d2;
    private AtomicBoolean d3;
    private AtomicBoolean disabler;

    private NeuronI createNeuron() {
        return new NeuronI(new ConstStatisticRule(3), new ConstStatisticRule(3), executionManager);
    }

    @BeforeEach
    public void setup() {
        executionManager = new NeuronExecutionManager(3);
        d1 = new AtomicBoolean(false);
        d2 = new AtomicBoolean(false);
        d3 = new AtomicBoolean(false);
        disabler = new AtomicBoolean(false);
    }

    private Link createDetectorLink(NeuronI n) {
        return LinkFactory.detector(d1, n);
    }


    private Link createDetectorLink(AtomicBoolean d, NeuronI n) {
        return LinkFactory.detector(d, n);
    }

    private Link createDisablerLink(NeuronI n) {
        return LinkFactory.disabler(n, disabler);
    }

    @Test
    public void testOneNeuron() {
        NeuronI n1 = createNeuron();
        n1.addInputLink(createDetectorLink(n1));
        n1.addOutputLink(createDisablerLink(n1));

        step(1);
        assertNeuron(false, false, 0, 0, n1);

        step(2);
        d1.set(true);
        process(n1);
        assertNeuron(false, false, 1, 0, n1);

        step(3);
        process(n1);
        assertNeuron(false, false, 2, 0, n1);

        step(4);
        process(n1);
        assertNeuron(false, false, 3, 1, n1);

        step(5);
        process(n1);
        assertNeuron(true, false, 4, 2, n1);

        step(6);
        process(n1);
        assertNeuron(true, false, 5, 3, n1);

        step(7);
        d1.set(false);
        process(n1);
        assertNeuron(true, true, 5, 3, n1);

        step(8);
        process(n1);
        assertNeuron(true, false, 5, 3, n1);

        step(9);
        disabler.set(true);
        process(n1);
        assertNeuron(true, false, 5, 3, n1);

        step(10);
        disabler.set(true);
        process(n1);
        assertNeuron(false, false, 5, 3, n1);
    }

    @Test
    public void testTwoNeuron() {
        NeuronI n1 = createNeuron();
        NeuronI n2 = createNeuron();

        n1.addInputLink(createDetectorLink(n1));
        n2.addOutputLink(createDisablerLink(n2));

        n1.supplyTo(n2);
        Assertions.assertEquals(1, n1.getIncomeLinks().size());
        Assertions.assertEquals(1, n1.getOutgoingLinks().size());
        Assertions.assertEquals(1, n2.getIncomeLinks().size());
        Assertions.assertEquals(1, n2.getOutgoingLinks().size());
        Assertions.assertTrue(n1.getIncomeLinks().stream().anyMatch(link -> link.getSender() == d1));
        Assertions.assertTrue(n1.getOutgoingLinks().stream().anyMatch(link -> link.getReceiver().equals(n2)));
        Assertions.assertTrue(n2.getIncomeLinks().stream().anyMatch(link -> link.getSender().equals(n1)));
        Assertions.assertTrue(n2.getOutgoingLinks().stream().anyMatch(link -> link.getReceiver() == disabler));

        step(1);
        assertNeuron(false, false, 0, 0, n1);
        assertNeuron(false, false, 0, 0, n2);

        step(2);
        d1.set(true);
        process(n1);
        assertNeuron(false, false, 1, 0, n1);
        assertNeuron(false, false, 0, 0, n2);

        step(3);
        process(n1);
        assertNeuron(false, false, 2, 0, n1);
        assertNeuron(false, false, 0, 0, n2);

        step(4);
        process(n1);
        assertNeuron(false, false, 3, 1, n1);
        assertNeuron(false, false, 0, 0, n2);

        step(5);
        process(n1);
        assertNeuron(true, false, 4, 2, n1);
        assertNeuron(false, false, 1, 0, n2);

        step(6);
        process(n1);
        assertNeuron(true, false, 5, 3, n1);
        assertNeuron(false, false, 2, 0, n2);

        step(7);
        d1.set(false);
        executionManager.tick();
        assertNeuron(true, true, 5, 3, n1);
        assertNeuron(false, false, 3, 1, n2);

        step(8);
        executionManager.tick();
        assertNeuron(true, false, 5, 3, n1);
        assertNeuron(true, false, 4, 2, n2);

        step(9);
        executionManager.tick();
        assertNeuron(true, false, 5, 3, n1);
        assertNeuron(true, false, 5, 3, n2);

        step(10);
        // Возможно, если отключающий сигнал обработается позже, чем будет отправлен дата сигнал
        // все сломается
        executionManager.tick();
        assertNeuron(true, false, 5, 3, n1);
        assertNeuron(true, true, 6, 4, n2);

        step(11);
        executionManager.tick();
        assertNeuron(false, false, 5, 3, n1);
        assertNeuron(true, true, 6, 4, n2);

        step(12);
        disabler.set(true);
        executionManager.tick();
        assertNeuron(false, false, 5, 3, n1);
        assertNeuron(true, false, 6, 4, n2);
    }

    @Test
    public void testNetwork() {
        NeuronI n1 = createNeuron();
        NeuronI n2 = createNeuron();
        NeuronI n3 = createNeuron();

        NeuronI n4 = createNeuron();
        NeuronI n5 = createNeuron();
        NeuronI n6 = createNeuron();

        NeuronI n7 = createNeuron();

        n1.addInputLink(createDetectorLink(d1, n1));
        n2.addInputLink(createDetectorLink(d2, n2));
        n3.addInputLink(createDetectorLink(d3, n3));

        log.trace("----LAYER 1---");
        n1.supplyTo(n4);
        n2.supplyTo(n5);
        n3.supplyTo(n6);

        log.trace("----LAYER 2---");
        n4.supplyTo(n7);
        n5.supplyTo(n7);
        n6.supplyTo(n7);

        // check structure
        Assertions.assertEquals(1, n1.getIncomeLinks().size());
        Assertions.assertSame(d1, n1.getIncomeLinks().get(0).getSender());
        Assertions.assertEquals(1, n1.getOutgoingLinks().size());
        Assertions.assertEquals(n4, n1.getOutgoingLinks().get(0).getReceiver());

        Assertions.assertEquals(1, n2.getIncomeLinks().size());
        Assertions.assertSame(d2, n2.getIncomeLinks().get(0).getSender());
        Assertions.assertEquals(1, n2.getOutgoingLinks().size());
        Assertions.assertEquals(n5, n2.getOutgoingLinks().get(0).getReceiver());

        Assertions.assertEquals(1, n3.getIncomeLinks().size());
        Assertions.assertSame(d3, n3.getIncomeLinks().get(0).getSender());
        Assertions.assertEquals(1, n3.getOutgoingLinks().size());
        Assertions.assertEquals(n6, n3.getOutgoingLinks().get(0).getReceiver());

        Assertions.assertEquals(1, n4.getIncomeLinks().size());
        Assertions.assertEquals(n1, n4.getIncomeLinks().get(0).getSender());
        Assertions.assertEquals(1, n4.getOutgoingLinks().size());
        Assertions.assertEquals(n7, n4.getOutgoingLinks().get(0).getReceiver());

        Assertions.assertEquals(1, n5.getIncomeLinks().size());
        Assertions.assertEquals(n2, n5.getIncomeLinks().get(0).getSender());
        Assertions.assertEquals(1, n5.getOutgoingLinks().size());
        Assertions.assertEquals(n7, n5.getOutgoingLinks().get(0).getReceiver());

        Assertions.assertEquals(1, n6.getIncomeLinks().size());
        Assertions.assertEquals(n3, n6.getIncomeLinks().get(0).getSender());
        Assertions.assertEquals(1, n6.getOutgoingLinks().size());
        Assertions.assertEquals(n7, n6.getOutgoingLinks().get(0).getReceiver());

        // check signals
        // 1
        step(1);
        assertNeuron(false, false, 0, 0, n1);
        assertNeuron(false, false, 0, 0, n2);
        assertNeuron(false, false, 0, 0, n3);
        assertNeuron(false, false, 0, 0, n4);
        assertNeuron(false, false, 0, 0, n5);
        assertNeuron(false, false, 0, 0, n6);
        assertNeuron(false, false, 0, 0, n7);

        // 2
        step(2);
        setDetectors(true, true, true);
        process(n1, n2, n3);
        assertNeuron(false, false, 1, 0, n1);
        assertNeuron(false, false, 1, 0, n2);
        assertNeuron(false, false, 1, 0, n3);
        assertNeuron(false, false, 0, 0, n4);
        assertNeuron(false, false, 0, 0, n5);
        assertNeuron(false, false, 0, 0, n6);
        assertNeuron(false, false, 0, 0, n7);

        // 3
        step(3);
        process(n1, n2, n3);
        assertNeuron(false, false, 2, 0, n1);
        assertNeuron(false, false, 2, 0, n2);
        assertNeuron(false, false, 2, 0, n3);
        assertNeuron(false, false, 0, 0, n4);
        assertNeuron(false, false, 0, 0, n5);
        assertNeuron(false, false, 0, 0, n6);
        assertNeuron(false, false, 0, 0, n7);

        // 4
        step(4);
        process(n1, n2, n3);
        assertNeuron(false, false, 3, 1, n1);
        assertNeuron(false, false, 3, 1, n2);
        assertNeuron(false, false, 3, 1, n3);
        assertNeuron(false, false, 0, 0, n4);
        assertNeuron(false, false, 0, 0, n5);
        assertNeuron(false, false, 0, 0, n6);
        assertNeuron(false, false, 0, 0, n7);

        // 5
        step(5);
        process(n1, n2, n3);
        assertNeuron(true, false, 4, 2, n1);
        assertNeuron(true, false, 4, 2, n2);
        assertNeuron(true, false, 4, 2, n3);
        assertNeuron(false, false, 1, 0, n4);
        assertNeuron(false, false, 1, 0, n5);
        assertNeuron(false, false, 1, 0, n6);
        assertNeuron(false, false, 0, 0, n7);

        // 6
        step(6);
        process(n1, n2, n3);
        assertNeuron(true, false, 5, 3, n1);
        assertNeuron(true, false, 5, 3, n2);
        assertNeuron(true, false, 5, 3, n3);
        assertNeuron(false, false, 2, 0, n4);
        assertNeuron(false, false, 2, 0, n5);
        assertNeuron(false, false, 2, 0, n6);
        assertNeuron(false, false, 0, 0, n7);

        // 7
        step(7);
        process(n1, n2, n3);
        assertNeuron(true, true, 6, 4, n1);
        assertNeuron(true, true, 6, 4, n2);
        assertNeuron(true, true, 6, 4, n3);
        assertNeuron(false, false, 3, 1, n4); // нейроны уже послали сигнал, который будет активен
        assertNeuron(false, false, 3, 1, n5); // при следующем тике
        assertNeuron(false, false, 3, 1, n6);
        assertNeuron(false, false, 0, 0, n7);

        // 8
        step(8);
        process(n1, n2, n3);
        assertNeuron(true, true, 7, 5, n1);
        assertNeuron(true, true, 7, 5, n2);
        assertNeuron(true, true, 7, 5, n3);
        assertNeuron(true, false, 4, 2, n4);
        assertNeuron(true, false, 4, 2, n5);
        assertNeuron(true, false, 4, 2, n6);
        assertNeuron(false, false, 1, 0, n7);

        // 9
        step(9);
        process(n1, n2, n3);
        assertNeuron(true, true, 8, 6, n1);
        assertNeuron(true, true, 8, 6, n2);
        assertNeuron(true, true, 8, 6, n3);
        assertNeuron(true, false, 5, 3, n4);  // нейрон отправляет отключающий сигнал
        assertNeuron(true, false, 5, 3, n5);
        assertNeuron(true, false, 5, 3, n6);
        assertNeuron(false, false, 2, 0, n7);

        // 10
        step(10);
        process(n1, n2, n3);
        assertNeuron(true, true, 9, 7, n1);
        assertNeuron(true, true, 9, 7, n2);
        assertNeuron(true, true, 9, 7, n3);
        assertNeuron(true, true, 6, 4, n4);  // нейрон должен послать отключающий сигнал
        assertNeuron(true, true, 6, 4, n5);  // в ответ на пишедкий дата сигнал
        assertNeuron(true, true, 6, 4, n6);
        assertNeuron(false, false, 3, 1, n7);

        // 11
        step(11);
        setDetectors(false, false, false);
        // На нейроны 1, 2, 3 должны прийти одновременно отключающие и дата сигналы
        // отключающий сигнал является главным,
        // поэтому в этот момент времени нейрон должен быть выключен и не посылать сигналов
        executionManager.tick();
        assertNeuron(false, true, 9, 7, n1);
        assertNeuron(false, true, 9, 7, n2);
        assertNeuron(false, true, 9, 7, n3);
        assertNeuron(true, true, 6, 4, n4);
        assertNeuron(true, true, 6, 4, n5);
        assertNeuron(true, true, 6, 4, n6);
        assertNeuron(true, false, 4, 2, n7);

        step(12);
        executionManager.tick();
        assertNeuron(false, false, 9, 7, n1);
        assertNeuron(false, false, 9, 7, n2);
        assertNeuron(false, false, 9, 7, n3);
        assertNeuron(true, false, 6, 4, n4);
        assertNeuron(true, false, 6, 4, n5);
        assertNeuron(true, false, 6, 4, n6);
        assertNeuron(true, false, 5, 3, n7);

        step(13);
        executionManager.tick();
        assertNeuron(false, false, 9, 7, n1);
        assertNeuron(false, false, 9, 7, n2);
        assertNeuron(false, false, 9, 7, n3);
        assertNeuron(true, false, 6, 4, n4);
        assertNeuron(true, false, 6, 4, n5);
        assertNeuron(true, false, 6, 4, n6);
        assertNeuron(true, true, 6, 4, n7);

        step(14);
        setDetectors(true, true, true);
        process(n1, n2, n3);
        assertNeuron(false, false, 10, 8, n1);
        assertNeuron(false, false, 10, 8, n2);
        assertNeuron(false, false, 10, 8, n3);
        assertNeuron(false, false, 6, 4, n4);
        assertNeuron(false, false, 6, 4, n5);
        assertNeuron(false, false, 6, 4, n6);
        assertNeuron(true, true, 6, 4, n7);

        step(15);
        process(n1, n2, n3);
        assertNeuron(true, true, 11, 9, n1);
        assertNeuron(true, true, 11, 9, n2);
        assertNeuron(true, true, 11, 9, n3);
        assertNeuron(false, false, 7, 5, n4);
        assertNeuron(false, false, 7, 5, n5);
        assertNeuron(false, false, 7, 5, n6);
        assertNeuron(true, false, 6, 4, n7);

        step(16);
        process(n1, n2, n3);
        assertNeuron(true, true, 12, 10, n1);
        assertNeuron(true, true, 12, 10, n2);
        assertNeuron(true, true, 12, 10, n3);
        assertNeuron(true, true, 8, 6, n4);
        assertNeuron(true, true, 8, 6, n5);
        assertNeuron(true, true, 8, 6, n6);
        assertNeuron(true, false, 7, 5, n7);

        step(17);
        process(n1, n2, n3);
        assertNeuron(false, true, 13, 11, n1);
        assertNeuron(false, true, 13, 11, n2);
        assertNeuron(false, true, 13, 11, n3);
        assertNeuron(true, true, 8, 6, n4);
        assertNeuron(true, true, 8, 6, n5);
        assertNeuron(true, true, 8, 6, n6);
        assertNeuron(true, true, 8, 6, n7);

        step(18);
        process(n1, n2, n3);
        assertNeuron(false, true, 14, 12, n1);
        assertNeuron(false, true, 14, 12, n2);
        assertNeuron(false, true, 14, 12, n3);
        assertNeuron(false, false, 8, 6, n4);
        assertNeuron(false, false, 8, 6, n5);
        assertNeuron(false, false, 8, 6, n6);
        assertNeuron(true, true, 8, 6, n7);

        step(19);
        process(n1, n2, n3);
        assertNeuron(true, true, 15, 13, n1);
        assertNeuron(true, true, 15, 13, n2);
        assertNeuron(true, true, 15, 13, n3);
        assertNeuron(false, false, 9, 7, n4);
        assertNeuron(false, false, 9, 7, n5);
        assertNeuron(false, false, 9, 7, n6);
        assertNeuron(true, false, 8, 6, n7);

        step(20);
        process(n1, n2, n3);
        assertNeuron(true, true, 16, 14, n1);
        assertNeuron(true, true, 16, 14, n2);
        assertNeuron(true, true, 16, 14, n3);
        assertNeuron(true, true, 10, 8, n4);
        assertNeuron(true, true, 10, 8, n5);
        assertNeuron(true, true, 10, 8, n6);
        assertNeuron(true, false, 9, 7, n7);

        step(21);
        setDetectors(false, false, false);
        executionManager.tick();
        assertNeuron(false, true, 16, 14, n1);
        assertNeuron(false, true, 16, 14, n2);
        assertNeuron(false, true, 16, 14, n3);
        assertNeuron(true, true, 10, 8, n4);
        assertNeuron(true, true, 10, 8, n5);
        assertNeuron(true, true, 10, 8, n6);
        assertNeuron(true, true, 10, 8, n7);

        step(22);
        executionManager.tick();
        assertNeuron(false, false, 16, 14, n1);
        assertNeuron(false, false, 16, 14, n2);
        assertNeuron(false, false, 16, 14, n3);
        assertNeuron(false, false, 10, 8, n4);
        assertNeuron(false, false, 10, 8, n5);
        assertNeuron(false, false, 10, 8, n6);
        assertNeuron(true, true, 10, 8, n7);

        step(23);
        setDetectors(true, true, false);
        process(n1, n2);
        assertNeuron(false, false, 17, 15, n1);
        assertNeuron(false, false, 17, 15, n2);
        assertNeuron(false, false, 16, 14, n3);
        assertNeuron(false, false, 10, 8, n4);
        assertNeuron(false, false, 10, 8, n5);
        assertNeuron(false, false, 10, 8, n6);
        assertNeuron(true, false, 10, 8, n7);

        step(24);
        process(n1, n2);
        assertNeuron(true, true, 18, 16, n1);
        assertNeuron(true, true, 18, 16, n2);
        assertNeuron(false, false, 16, 14, n3);
        assertNeuron(false, false, 11, 9, n4);
        assertNeuron(false, false, 11, 9, n5);
        assertNeuron(false, false, 10, 8, n6);
        assertNeuron(true, false, 10, 8, n7);

        step(25);
        process(n1, n2);
        assertNeuron(true, true, 19, 17, n1);
        assertNeuron(true, true, 19, 17, n2);
        assertNeuron(false, false, 16, 14, n3);
        assertNeuron(true, true, 12, 10, n4);
        assertNeuron(true, true, 12, 10, n5);
        assertNeuron(false, false, 10, 8, n6);
        assertNeuron(true, false, 10, 8, n7);
    }

    private void step(int i) {
        log.info("STEP {}", i);
    }

    private void setDetectors(boolean d1Val, boolean d2Val, boolean d3Val) {
        d1.set(d1Val);
        d2.set(d2Val);
        d3.set(d3Val);
    }

    private void process(NeuronI... neurons) {
        Arrays.stream(neurons).forEach(n -> {
            executionManager.scheduleNeuron(n);
        });
        executionManager.tick();
    }

    private void assertNeuron(boolean outSignal, boolean disableSignal, int ruleL, int ruleG, NeuronI n1) {
        Assertions.assertEquals(outSignal, n1.getOutSignal());
        Assertions.assertEquals(disableSignal, n1.getDisableSignal());
        Assertions.assertEquals(ruleL, n1.getRuleL().getCount());
        Assertions.assertEquals(ruleG, n1.getRuleG().getCount());
    }
}
