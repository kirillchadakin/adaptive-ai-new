package mord;

import reactor.core.publisher.ConnectableFlux;
import reactor.core.publisher.Flux;
import reactor.core.publisher.FluxSink;

import java.time.Duration;

/**
 * Hello world!
 */
public class App {
    private static FluxSink<Long> emitSink;

    public static void main(String[] args) throws InterruptedException {
        ConnectableFlux<Long> publish = Flux.<Long>create(sink -> emitSink = sink).publish();
        publish.connect();
        Flux.interval(Duration.ofSeconds(1)).subscribe(i -> emitSink.next(i));
        publish.subscribe(System.out::println);
        Thread.sleep(3000);
        publish.subscribe(System.out::println);
        Thread.sleep(10_000);
    }
}
