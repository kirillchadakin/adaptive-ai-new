package mord.ai.stat;

public interface StatisticRule {
    boolean check();
    long getCount();
}
