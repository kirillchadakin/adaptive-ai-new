package mord.ai.stat;

import lombok.Getter;

import java.util.StringJoiner;

public class ConstStatisticRule implements StatisticRule {
    private final int threshold;
    private int count;
    private boolean isThresholdBroken = false;

    public ConstStatisticRule(int threshold) {
        this.threshold = threshold;
    }

    @Override
    public boolean check() {
        count++;
        if (isThresholdBroken) {
            return true;
        }
        isThresholdBroken = count >= threshold;
        return isThresholdBroken;
    }

    @Override
    public long getCount() {
        return count;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", ConstStatisticRule.class.getSimpleName() + "[", "]")
                .add("threshold=" + threshold)
                .add("count=" + count)
                .add("isThresholdBroken=" + isThresholdBroken)
                .toString();
    }
}
