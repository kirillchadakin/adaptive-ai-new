package mord.ai;

import java.util.function.Consumer;

public interface Neuron<D> extends Consumer<D> {
}
