package mord.ai;

import lombok.Getter;
import lombok.extern.log4j.Log4j2;
import mord.ai.execution.NeuronExecutionManager;
import mord.ai.link.Link;
import mord.ai.link.LinkFactory;
import mord.ai.stat.StatisticRule;

import java.util.Collections;
import java.util.List;
import java.util.StringJoiner;
import java.util.concurrent.CopyOnWriteArrayList;

@Log4j2
public class NeuronI implements Runnable {
    @Getter
    private final Reference reference = Reference.create(this);

    private final NeuronExecutionManager executionManager;

    @Getter
    private final StatisticRule ruleL;

    @Getter
    private final StatisticRule ruleG;

    private final List<Link> incomeLinks = new CopyOnWriteArrayList<>();
    private final List<Link> outgoingLinks = new CopyOnWriteArrayList<>();
    private final ResetTask resetTask = new ResetTask();

    private boolean outSignal = false;
    private boolean nextOutSignal = false;
    private boolean disableSignal = false;
    private boolean nextDisableSignal = false;

    public NeuronI(StatisticRule ruleL, StatisticRule ruleG, NeuronExecutionManager executionManager) {
        this.executionManager = executionManager;
        this.ruleL = ruleL;
        this.ruleG = ruleG;
    }

    public void supplyTo(NeuronI neuron) {
        Link link = LinkFactory.neuronToNeuron(this, neuron);
        this.outgoingLinks.add(link);
        neuron.incomeLinks.add(link);
        // TODO: 06.05.2023 Перевести все логирование на аспекты
        log.debug("{} connected to {}", this, neuron);
    }

    // FIXME: 10.04.2023 more general
    public void addInputLink(Link link) {
        incomeLinks.add(link);
        log.debug("{} added input link: {}", this, link);
    }

    public void addOutputLink(Link link) {
        outgoingLinks.add(link);
        log.debug("{} added output link: {}", this, link);
    }

    public boolean getOutSignal() {
        return outSignal;
    }

    public boolean getDisableSignal() {
        return disableSignal;
    }

    /**
     * Пересчитывает выходные параметры нейрона
     */
    @Override
    public void run() {
        processInputSignals();

        if (nextOutSignal) {
            // Если сигнал был отправлен, необходимо пересчитать нейроны-получателя после получения сигнала (приоритет 1)
            outgoingLinks.forEach(link -> scheduleProcessingOnNextTick(link.getReceiver()));
        }

        if (nextDisableSignal) {
            // Если сигнал был отправлен, необходимо пересчитать нейроны-получателя после получения сигнала (приоритет 1)
            incomeLinks.forEach(link -> scheduleProcessingOnNextTick(link.getSender()));
        }

        if (nextOutSignal || nextDisableSignal) {
            // Если был отправлен сигнал, то он может быть отправлен на следующем тике, если не был получен отключающий
            // поэтому запускаем обрабаботку у текущего нейрона на следующем шаге после получнеия сигналов (приоритет 1)
            // Для текущей реализации нужно выключить отключающий сигнал на следующем тике т к он отправляется 1 раз
            scheduleProcessingOnNextTick(this);
        }

        // Сигнали обновятся (отправятся) только на следующем тике перед обработкой другими нейронами (приоритет 0)
        executionManager.schedule(resetTask);

        log.debug("Inputs processed for {}", this);
    }

    private void scheduleProcessingOnNextTick(Object processable) {
        if (processable instanceof Runnable) {
            if (processable instanceof NeuronI) {
                executionManager.scheduleNeuron((NeuronI) processable);
            } else {
                executionManager.schedule((Runnable) processable, 1);
            }
        }
    }

    private void processInputSignals() {
        boolean l = structureRule() && ruleL.check();
        nextDisableSignal = l && ruleG.check();
        nextOutSignal = outTrigger(l);
    }

    public void reset() {
        log.trace("Reset {}: nextOut={}, out={}, nextDisable={}, disable={}", reference.getId(),
                signalAsNumber(nextOutSignal), signalAsNumber(outSignal), signalAsNumber(nextDisableSignal), signalAsNumber(disableSignal));
        outSignal = nextOutSignal;
        disableSignal = nextDisableSignal;
    }

    private boolean structureRule() {
        boolean isPassed = incomeLinks.stream().allMatch(Link::getInput);
        log.debug(isPassed ? "Structure rule is passed for {}" : "Structure rule is failed for {}", reference.getId());
        return isPassed;
    }

    private boolean outTrigger(boolean l) {
        boolean disabled = outgoingLinks.stream().anyMatch(Link::getDisable);
        boolean result = !disabled && (outSignal || l);
        log.trace("Next out signal calculation for {}: disabled={}, out={}, ruleL={}, result={}.", reference.getId(),
                signalAsNumber(disabled), signalAsNumber(outSignal), signalAsNumber(l), signalAsNumber(result));
        return result;
    }

    public List<Link> getIncomeLinks() {
        return Collections.unmodifiableList(incomeLinks);
    }

    public List<Link> getOutgoingLinks() {
        return Collections.unmodifiableList(outgoingLinks);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        NeuronI neuronI = (NeuronI) o;
        return reference.equals(neuronI.reference);
    }

    @Override
    public int hashCode() {
        return reference.hashCode();
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", NeuronI.class.getSimpleName() + "[", "]")
                .add("id=" + reference.getId())
                .add("out=" + signalAsNumber(outSignal))
                .add("disable=" + signalAsNumber(disableSignal))
                .add("L=" + ruleL.getCount())
                .add("G=" + ruleG.getCount())
                .toString();
    }

    private int signalAsNumber(boolean sign) {
        return sign ? 1 : 0;
    }

    public class ResetTask implements Runnable {
        @Override
        public void run() {
            reset();
        }

        @Override
        public String toString() {
            return "Neuron %d reset".formatted(reference.getId());
        }
    }
 }
