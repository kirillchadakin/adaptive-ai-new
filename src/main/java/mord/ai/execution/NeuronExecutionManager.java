package mord.ai.execution;

import lombok.extern.log4j.Log4j2;
import mord.ai.NeuronI;

import java.util.HashSet;
import java.util.Set;

// TODO: 01.05.2023 Использовать внедренеие вместо наследования
@Log4j2
public class NeuronExecutionManager extends ExecutionManager {
    private final Set<NeuronI> scheduledNeurons = new HashSet<>();

    public NeuronExecutionManager(int priorityLevels) {
        super(priorityLevels);
    }

    public void scheduleNeuron(NeuronI neuronI) {
        if (scheduledNeurons.add(neuronI)) {
            super.schedule(neuronI, 1);
        } else {
            log.info("Scheduling {} is cansled (already scheduled)", neuronI);
        }
    }

    @Override
    public void tick() {
        // Очищаем добавленные для выполнения нейроны до выполнения
        // в текущем тике могут быть добавлены те же самые нейроны
        scheduledNeurons.clear();
        super.tick();
//        log.info("Scheduled neurons are cleared: {}", scheduledNeurons);
    }
}
