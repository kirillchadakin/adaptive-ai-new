package mord.ai.execution;

import lombok.extern.log4j.Log4j2;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Log4j2
public class ExecutionManager {
    private static final int BUFFER_AMOUNT = 2;
    private final Object bufferLock = new Object();
    private List<Set<Runnable>> executions;
    private final List<List<Set<Runnable>>> buffers;
    private int currentBufferIndex = 0;

    public ExecutionManager(int priorityLevels) {
        buffers = IntStream.range(0, BUFFER_AMOUNT)
                .mapToObj(i -> createBuffer(priorityLevels))
                .collect(Collectors.toList());
        executions = buffers.get(currentBufferIndex);
    }

    private List<Set<Runnable>> createBuffer(int priorityLevels) {
        return IntStream.range(0, priorityLevels)
                .mapToObj(i -> new HashSet<Runnable>())
                .collect(Collectors.toList());
    }

    public void schedule(Runnable task) {
        schedule(task, 0);
    }

    public void schedule(Runnable task, int priority) {
        synchronized (bufferLock) {
            executions.get(priority).add(task);
            log.trace("Added to execution {}", task);
        }
    }

    // Тики не должны запускаться параллельно!
    public void tick() {
        List<Set<Runnable>> currentExecutions;
        synchronized (bufferLock) {
            currentExecutions = executions;
            currentBufferIndex = (currentBufferIndex + 1) % buffers.size();
            executions = buffers.get(currentBufferIndex);
        }
        // FIXME: 04.04.2023 Операции должны выполняться в отдельных потоках
        int i = 0;
        for (Set<Runnable> execs : currentExecutions) {
            log.info("Executing {} priority", i++);
            execs.forEach(exec -> {
                log.trace("Started execution {}", exec);
                exec.run();
                log.trace("Ended execution {}", exec);
            });
        }
        currentExecutions.forEach(Set::clear);
    }
}














