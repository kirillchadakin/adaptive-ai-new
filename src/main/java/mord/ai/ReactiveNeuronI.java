package mord.ai;

import lombok.extern.log4j.Log4j2;
import mord.ai.stat.StatisticRule;
import reactor.core.publisher.ConnectableFlux;
import reactor.core.publisher.Flux;
import reactor.core.publisher.FluxSink;

@Log4j2
public class ReactiveNeuronI {
    private final Reference reference = Reference.create(this);

    private final StatisticRule ruleL;
    private final StatisticRule ruleG;

    private FluxSink<Long> emitSink;
    private final ConnectableFlux<Long> emitter;


    public ReactiveNeuronI(StatisticRule ruleL, StatisticRule ruleG) {
        this.ruleL = ruleL;
        this.ruleG = ruleG;
        emitter = Flux.<Long>create(sink -> emitSink = sink).publish();
    }

    public void supplyTo(ReactiveNeuronI neuronI) {
        emitter.subscribe(neuronI::process);
    }

    private void process(Long val) {
        log.debug("{} receive signal.", this);
//        if (structureRule() && ruleL.check()) {
            if (ruleG.check()) {
//                sendDisableSignal();
            }
//            sendDataSignal();
//        }
    }

//    private boolean structureRule() {
//        return incomeLinks.stream().allMatch(NeuronI::isActive);
//    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ReactiveNeuronI neuronI = (ReactiveNeuronI) o;
        return reference.equals(neuronI.reference);
    }

    @Override
    public int hashCode() {
        return reference.hashCode();
    }
}
