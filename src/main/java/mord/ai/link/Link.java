package mord.ai.link;

public interface Link {
    /**
     * Значение одного элемента входного вектора нейрона (X)
     */
    boolean getInput();

    /**
     * Значение одного элемента отключающего вектора нейрона (Z)
     * @return
     */
    boolean getDisable();

    // TODO: 10.04.2023 need interface
    Object getSender();

    Object getReceiver();
}
