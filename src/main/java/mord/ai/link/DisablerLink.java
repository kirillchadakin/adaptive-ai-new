package mord.ai.link;

import mord.ai.NeuronI;

import java.util.concurrent.atomic.AtomicBoolean;

public class DisablerLink implements Link {
    private final NeuronI outNeuron;
    private final AtomicBoolean disabler;

    public DisablerLink(NeuronI outNeuron, AtomicBoolean disabler) {
        this.outNeuron = outNeuron;
        this.disabler = disabler;
    }

    @Override
    public boolean getInput() {
        return outNeuron.getOutSignal();
    }

    @Override
    public boolean getDisable() {
        return disabler.get();
    }

    @Override
    public Object getSender() {
        return outNeuron;
    }

    @Override
    public Object getReceiver() {
        return disabler;
    }
}
