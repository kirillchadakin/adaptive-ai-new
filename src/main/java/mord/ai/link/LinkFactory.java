package mord.ai.link;

import lombok.experimental.UtilityClass;
import mord.ai.NeuronI;

import java.util.concurrent.atomic.AtomicBoolean;

@UtilityClass
public class LinkFactory {
    public Link neuronToNeuron(NeuronI senderNeuron, NeuronI receiverNeuron) {
        return new NeuronToNeuronLink(senderNeuron, receiverNeuron);
    }

    public Link detector(AtomicBoolean detector, NeuronI neuron) {
        return new DetectorLink(detector, neuron);
    }

    public Link disabler(NeuronI neuronI, AtomicBoolean disabler) {
        return new DisablerLink(neuronI, disabler);
    }
}
