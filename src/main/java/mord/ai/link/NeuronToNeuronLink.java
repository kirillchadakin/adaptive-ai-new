package mord.ai.link;

import mord.ai.NeuronI;

import java.util.StringJoiner;

public class NeuronToNeuronLink implements Link {
    private final NeuronI senderNeuron;
    private final NeuronI receiverNeuron;

    public NeuronToNeuronLink(NeuronI senderNeuron, NeuronI receiverNeuron) {
        this.receiverNeuron = receiverNeuron;
        this.senderNeuron = senderNeuron;
    }

    @Override
    public boolean getInput() {
        return senderNeuron.getOutSignal();
    }

    @Override
    public boolean getDisable() {
        return receiverNeuron.getDisableSignal();
    }

    @Override
    public Object getSender() {
        return senderNeuron;
    }

    @Override
    public Object getReceiver() {
        return receiverNeuron;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Link.class.getSimpleName() + "[", "]")
                .add(senderNeuron.toString())
                .add(receiverNeuron.toString())
                .toString();
    }
}
