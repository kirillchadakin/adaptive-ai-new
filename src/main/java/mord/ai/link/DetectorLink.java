package mord.ai.link;

import mord.ai.NeuronI;

import java.util.concurrent.atomic.AtomicBoolean;

public class DetectorLink implements Link{
    private final AtomicBoolean detector;
    private final NeuronI neuronI;

    public DetectorLink(AtomicBoolean detector, NeuronI neuronI) {
        this.detector = detector;
        this.neuronI = neuronI;
    }

    @Override
    public boolean getInput() {
        return detector.get();
    }

    @Override
    public boolean getDisable() {
        return neuronI.getDisableSignal();
    }

    @Override
    public Object getSender() {
        return detector;
    }

    @Override
    public Object getReceiver() {
        return neuronI;
    }
}
