/**
 * (C) Turbonomic 2019.
 */

package mord.ai;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;

public class Reference {
    private static final AtomicLong identifierGenerator = new AtomicLong(1);  // 1 для тестов

    private final long id;
    private final Object instance;

    private Reference(long id, Object instance) {
        this.id = id;
        this.instance = instance;
    }

    public static Reference create(Object instance) {
        return new Reference(identifierGenerator.getAndIncrement(), instance);
    }

    public long getId() {
        return id;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof final Reference that)) {
            return false;
        }
        return id == that.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return String.format("%s [id=%s, instance=%s]", getClass().getSimpleName(),
                        this.id, this.instance.toString());
    }
}
